#include "CsoundHelper.hpp"
#include <iostream>


CsoundHelper::CsoundHelper() 
	: m_csound(new Csound)
{
	Reset();
}

CsoundHelper::~CsoundHelper()
{
	delete m_csound;
}

void CsoundHelper::Reset()
{
	m_csound->Reset();
	csoundSetGlobalEnv("OPCODE6DIR64", "C:\\Program Files\\Csound6_x64\\plugins64");
	m_csound->SetOption("-odac");
	m_orc = ("sr=44100\n\
    ksmps=32\n\
    nchnls=2\n\
    0dbfs=1\n\
    \n\
	giWave	ftgen 1, 0, 1024, 10, 1 \n\
	giWave	ftgen 2, 0, 1024, 10, 1, 0, .33, 0, .2, 0, .14, 0, .11, 0, .09 \n\
	giWave	ftgen 3, 0, 1024, 10, 10, 0, .2, 0, .4, 0, .6, 0, .8, 0, 1, 0, .8, 0, .6, 0, .4, 0, .2 \n\
    instr 1\n\
    aEnv linenr 0.5, .001, .1, .001 \n\
    aout oscili p6*aEnv,  cpsmidinn(p4), p5\n\
    outs aout, aout\n\
    endin\n");
	m_sco = "";
}

void CsoundHelper::SetScoreString(std::string score)
{
	m_sco = score;
}

void CsoundHelper::SaveFile(std::string fileName)
{
	std::string option = "-o " + fileName + ".wav";
	m_csound->SetOption(option.c_str());
}

void CsoundHelper::PlayScore()
{
	std::cout << m_sco.c_str();
	
	//compile orc
	m_csound->CompileOrc(m_orc.c_str());

	//compile sco
	m_csound->ReadScore(m_sco.c_str());

	//prepare Csound for performance
	m_csound->Start();

	//perform entire score
	m_csound->Perform();

	Reset();
}