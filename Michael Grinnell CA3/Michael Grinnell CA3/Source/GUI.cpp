﻿#include "GUI.hpp"

#include <stdio.h>
#include <iostream>

//Handles all the print functions
GUI::GUI() {}

void GUI::PrintMain()
{

	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\t-------- Michael Grinnell CA3 --------\n" << std::endl;
	std::cout << "\tPlease enter the number of the task you wish to run\n\n\t(0) To exit Application" << std::endl;
	std::cout << "\t(1) Create/Edit Score" << std::endl;
	std::cout << "\t(2) Play Score" << std::endl;
	std::cout << "\t(3) Save Score as Audio File" << std::endl;
	std::cout << "\t(4) Print Score Details" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintCreate()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease Choose the parameters you wish to modify\n\n\t(0) To save and return to main menu" << std::endl;

	std::cout << "\t(1) Instrument" << std::endl;
	std::cout << "\t(2) Key" << std::endl;
	std::cout << "\t(3) Scale" << std::endl;
	std::cout << "\t(4) Length" << std::endl;
	std::cout << "\t(5) Tempo" << std::endl;
	std::cout << "\t(6) Note Rate" << std::endl;
	std::cout << "\t(7) Note Length" << std::endl;
	std::cout << "\t(8) Score Style" << std::endl;
	std::cout << "\t(9) Score Octave" << std::endl;
	std::cout << "\t(10) Score Accent" << std::endl;
	std::cout << "\t(11) Score Amplitude Variation" << std::endl;
	std::cout << "\t(12) Reset Score" << std::endl;
	std::cout << "\t(13) Randomize!" << std::endl;

	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintSave()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease enter A filename and press enter to save\n\n"<< std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintScore(Score score)
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tThe Parameters of the Current Score" << std::endl;

	std::cout << score.ToString();

	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintInstruments()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease select the instrument you wish to use" << std::endl;

	std::cout << "\t(1) Sine Wave" << std::endl;
	std::cout << "\t(2) Odd Harmonics" << std::endl;
	std::cout << "\t(3) Even Harmonics" << std::endl;
	std::cout << "\t(4) Randomize!" << std::endl;

	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintKeys()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease select the Key you wish to use" << std::endl;
	std::cout << "\tValid Keys [C, C#, D, D#, E, F, F#, G, G#, A, A#, B]" << std::endl;

	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintScale()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease select the scale you wish to use" << std::endl;

	std::cout << "\t(1) Major" << std::endl;
	std::cout << "\t(2) Minor" << std::endl;
	std::cout << "\t(3) Custom Scale" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintCustomScale()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease enter the custom scale you wish to use" << std::endl;
	std::cout << "\tEach successive number will represent the distance from the root note" << std::endl;
	std::cout << "\tE.G. the major scale is 0,2,4,5,7,9,11,12 " << std::endl;
	std::cout << "\tYou must enter 8 notes" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintLength()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease enter the length of the score in seconds" << std::endl;
	std::cout << "\tValid Time is between 1 and 120 seconds" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintTempo()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease enter the Tempo of the score in Beats Per Minute" << std::endl;
	std::cout << "\tValid input is between 20 and 360 BPM" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintRate()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease ste the rate of notes pre beat" << std::endl;
	std::cout << "\tValid input is between 1 and 8" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintNoteLength()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease enter the length of the notes in the piece" << std::endl;
	std::cout << "\tValid input is between 1 and 100 representing a percentage" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintStyle()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease choose a style to play the scale notes" << std::endl;
	std::cout << "\t(1) UP" << std::endl;
	std::cout << "\t(2) DOWN" << std::endl;
	std::cout << "\t(3) UPDOWN" << std::endl;
	std::cout << "\t(4) DOWNUP" << std::endl;
	std::cout << "\t(5) Random" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintOctaves()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease select the ocatve of the score" << std::endl;
	std::cout << "\tValid range 1 - 8" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintAccent()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease specify what note you would like to be accented" << std::endl;
	std::cout << "\tEntering 0 will mean there is no accent applied" << std::endl;
	std::cout << "\tEntering 2 will mean every second note is accented" << std::endl;
	std::cout << "\tValid range 0 - 100" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintVolumeVariation()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tPlease enter the percentage of volume variation on each note" << std::endl;
	std::cout << "\tValid range 0 - 100" << std::endl;
	std::cout << "\tEntering 0 will mean each note will have the volume" << std::endl;
	std::cout << "\tEntering 100 will mean each note is randomly 100% louder or 100% quieter" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

void GUI::PrintRandomize()
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	std::cout << "\tRandomizing all parameters except for score length" << std::endl;
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}