#include "Utility.hpp"
#include "Constants.hpp"

#include <iostream>
#include <algorithm> 

//Ensures input is a number within range
//This is used for selecting options from menus
int GetNumericInput(const char* message, int min, int max)
{
	bool isValid = false;
	bool isNumber = false;
	bool isInRange = false;

	std::string input;

	while (!isValid)
	{
		std::cout << message;
		std::cout << "\tINPUT: ";
		std::cin >> input;

		if (is_number(input) && !(std::stoi(input) < min || std::stoi(input) > max))
			isValid = true;
		else
		{
			std::cout << "\n\tInvalid Input!\n ";
			std::cin.clear();
			std::cin.ignore();
		}
	}

	return std::stoi(input);
}

//https://stackoverflow.com/questions/4654636/how-to-determine-if-a-string-is-a-number-with-c
bool is_number(const std::string& s)
{
	return !s.empty() && std::find_if(s.begin(),
		s.end(), [](unsigned char c) { return !std::isdigit(c); }) == s.end();
}

//Iterates through the array of keys, c,c#, d etc..
//will then return the midi note value of that key, ignoring octaves, 0 - 11, c - b
short GetValidKeyIndex()
{
	short index = -1;
	std::string input;

	while (index == -1)
	{
		std::cout << "\tINPUT: ";
		std::cin >> input;

		//Convert input to Upper Case
		std::transform(input.begin(), input.end(), input.begin(), ::toupper);

		//Iterate through notes array
		std::vector<std::string>::const_iterator it;
		it = std::find(NOTES.begin(), NOTES.end(), input);

		//set index to position of found note
		if (it != NOTES.end())
			index = it - NOTES.begin();
		else
		{
			std::cout << "\n\tInvalid Input!\n ";
			std::cin.clear();
			std::cin.ignore();
		}
	}

	return index;
}