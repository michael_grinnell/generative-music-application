#include "Score.hpp"
#include <Constants.hpp>

Score::Score() :
	m_root(0),
	m_scale(MAJOR_SCALE_MIDI),
	m_instrument(1),
	m_length(6),
	m_tempo(120),
	m_randomInstrument(false),
	m_rate(2),
	m_noteLength(0.5f),
	m_style(Style::UPDOWN),
	m_octave(60),
	m_ampVariation(0.2f),
	m_accent(8),
	m_amplitude(0.5f)
{
}

#pragma region Setters

void Score::SetRoot(unsigned short root)
{
	m_root = root;
}

void Score::SetScale(const std::vector<unsigned short> scale)
{
	m_scale = scale;
}

void Score::SetInstrument(unsigned short instrument)
{
	m_instrument = instrument;
}

void Score::SetLength(float length)
{
	m_length = length;
}

void Score::SetTempo(float tempo)
{
	m_tempo = tempo;
}

void Score::SetRate(float rate)
{
	m_rate = rate;
}

void Score::SetNoteLength(float length)
{
	m_noteLength = length;
}

void Score::SetRandomInstrument(bool randomInstrument)
{
	m_randomInstrument = randomInstrument;
}

void Score::SetStyle(Style style)
{
	m_style = style;
}

void Score::SetOct(int oct)
{
	m_octave = oct;
}

void Score::SetAccent(int accent)
{
	m_accent = accent;
}

void Score::SetAmplitudeVariation(float variation)
{
	m_ampVariation = variation;
}

#pragma endregion

#pragma region Getters

unsigned short Score::GetRoot()
{
	return m_root;
}

std::vector<unsigned short> Score::GetScale()
{
	return m_scale;
}


unsigned short Score::GetInstrument()
{
	return m_instrument;
}

float Score::GetLength()
{
	return m_length;
}

float Score::GetTempo()
{
	return m_tempo;
}

float Score::GetBPS()
{
	return 60 / m_tempo;
}

float Score::GetNotes()
{
	return m_length * (m_tempo / 60);
}

float Score::GetRate()
{
	return m_rate;
}

float Score::GetNoteLength()
{
	return m_noteLength;
}

int Score::GetOct()
{
	return m_octave;
}

bool Score::isRandomInstrument()
{
	return m_randomInstrument;
}

Score::Style Score::GetStyle()
{
	return m_style;
}

float Score::GetAmplitude()
{
	return m_amplitude;
}

float Score::GetAmplitudeVariation()
{
	return m_amplitude * m_ampVariation;
}

unsigned short Score::GetAccent()
{
	return m_accent;
}

#pragma endregion

std::string Score::ToString()
{
	std::string instrument = "Sine Wave";

	switch (m_instrument)
	{
	case 2:
		instrument = "Odd Harmonics";
		break;
	case 3:
		instrument = "Even Harmonics";
		break;
	case 4:
		instrument = "Random";
		break;
	case 1:
	default:
		instrument = "Sine Wave";
		break;
	}

	std::string key = NOTES[m_root % 12];

	std::string scale = "Major";
	if (m_scale == MINOR_SCALE_MIDI)
		scale = "Minor";
	else if (m_scale == MAJOR_SCALE_MIDI)
		scale = "Major";
	else
		scale = "Custom";

	

	std::string length = std::to_string((int)m_length) + " Seconds";
	std::string BPM = std::to_string((int)m_tempo) + " BPM";
	std::string style = "UP";

	if (m_style == Style::DOWN)
		style = "DOWN";
	else if (m_style == Style::UPDOWN)
		style = "UPDOWN";
	else if (m_style == Style::DOWNUP)
		style = "DOWNUP";
	else if (m_style == Style::RANDOM)
		style = "RANDOM";

	std::string scoreString = 
		  "\n\tInstrument:\t" + instrument
		+ "\n\tKey:\t" + key
		+ "\n\tScale:\t" + scale
		+ "\n\tLength:\t" + length
		+ "\n\tTempo:\t" + BPM 
		+ "\n\tNote Rate:\t" + std::to_string((int)m_rate)
		+ "\n\tNote Length:\t" + std::to_string((int)m_length) + "%"
		+ "\n\tScore Style:\t" + style
		+ "\n\tScore Octave:\t" + std::to_string(m_octave / 12)
		+ "\n\tScore Accent Note:\t" + std::to_string(m_accent)
		+ "\n\tScore Amplitude Variation:\t" + std::to_string((int)(m_ampVariation * 100)) + "%\n\n";

	return scoreString;
}
