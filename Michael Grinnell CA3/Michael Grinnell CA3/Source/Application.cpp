#include "Application.hpp"
#include "Utility.hpp"
#include "Constants.hpp"

#include <string>
#include <iostream>
#include <stdlib.h>  
#include <time.h>     

Application::Application() :
	m_csoundHelper(CsoundHelper()),
	m_score(Score()),
	m_gui(GUI()),
	m_isRunning(true)
{
	//Seed random Function for duration of application runtime
	srand(time(NULL));
}

void Application::run()
{
	//Main loop
	while (m_isRunning)
	{
		m_gui.PrintMain();
		ProcessIput();
	}
}

#pragma region Main Menu

//Get User input for the main menu
void Application::ProcessIput()
{
	int choice = GetNumericInput("", 0, 4);

	switch (choice)
	{
	case 1:
		CreateScore();
		break;
	case 2:
		CompileScore();
		break;
	case 3:
		SaveWav();
		CompileScore();
		break;
	case 4:
		m_gui.PrintScore(m_score);
		break;
	case 0:
	default:
		std::cout << "Goodbye" << std::endl;
		m_isRunning = false;
		break;
	}

}

//Get the parameters from the score object and create score string from them
void Application::CompileScore()
{ 
	std::string sco = "";

	unsigned short instrument = m_score.GetInstrument();
	float length = m_score.GetLength();

	// Divide by 60 to get beats in a second
	// then multiply by length in seconds and rate to get the total notes
	float totalNotes = (m_score.GetTempo() / 60) * length * m_score.GetRate();

	//Length / Totalnotes will make every note finish when the next one starts
	//Note length is a percentage of this, this stops notes from overlapping
	float noteDuration = (length / totalNotes) * m_score.GetNoteLength();

	float startTime = 0;

	//Reverse is used for the style, to allow movement up and down the scale
	bool reverse = false;

	for (size_t i = 0; i < totalNotes; ++i)
	{
		//Random instrument will use a random instrument for each note
		if (m_score.isRandomInstrument())
			instrument = rand() % 3 + 1;

		//Create the score string
		sco.append("i1 " 
			+ std::to_string(startTime) + " " 
			+ std::to_string(noteDuration) + " " 
			+ std::to_string(GetMidiNote(i, &reverse)) + " "
			+ std::to_string(instrument) + " "
			+ std::to_string(GetNoteAmplitude(i)) +"\n");

		startTime += length / totalNotes;
	}

	m_csoundHelper.SetScoreString(sco);
	m_csoundHelper.PlayScore();
}

int Application::GetMidiNote(int i, bool *reverse)
{
	//Swap direction every full itteration of the scale
	if (i % 8 == 0)
		*reverse = !*reverse;

	return (m_score.GetScale()[GetScaleIndex(i, *reverse)] + m_score.GetOct()) + m_score.GetRoot();
}

int Application::GetScaleIndex(int i, bool reverse)
{
	switch (m_score.GetStyle())
	{
	case Score::Style::UP:
		//from 0 - 7
		return i % 8;
	case Score::Style::DOWN:
		//from 7 - 0
		return ((i % 8) - 7) * -1;
	case Score::Style::UPDOWN:
		//from 0 - 7, then 7 - 0
		return reverse ? i % 8 : ((i % 8) - 7) * -1;
	case Score::Style::DOWNUP:
		//from 7 - 0, then 0 - 7
		return reverse ? ((i % 8) - 7) * -1 : i % 8;
	case Score::Style::RANDOM:
		//Random index
		return rand() % 8;
	}
}

float Application::GetNoteAmplitude(int i)
{
	float variationPercentage = m_score.GetAmplitudeVariation();
	float noteAmplitude = m_score.GetAmplitude();

	//Increases the volume for the accented note
	if (m_score.GetAccent() != 0 && i % m_score.GetAccent() == 0)
		noteAmplitude += 0.3f;

	float newVariation = 0;

	//Vol * 10000 is to turn into a floating point number withe percision of 3
	if (variationPercentage != 0)
	{
		int variation = (rand() % (int)(variationPercentage * 10000));
		newVariation = (float)variation / 10000.f;
	}

	//random increase or decrease by variation
	if (rand() % 100 < 50)
		return noteAmplitude - newVariation;
	else
		return noteAmplitude + newVariation;
}

void Application::SaveWav()
{
	m_gui.PrintSave();

	std::string fileName;
	std::cout << "\tFile Name: ";
	std::cin >> fileName;
	m_csoundHelper.SaveFile(fileName);
}

#pragma endregion

#pragma region Create Score

//Menu for creating and editing score parameters
void Application::CreateScore()
{
	int choice = -1;
	while (choice)
	{
		m_gui.PrintCreate();
		choice = GetNumericInput("", 0, 13);

		switch (choice)
		{
		case 1:
			SetIntrument();
			break;
		case 2:
			SetKey();
			break;
		case 3:
			SetScale();
			break;
		case 4:
			SetLength();
			break;
		case 5:
			SetTempo();
			break;
		case 6:
			SetRate();
			break;
		case 7:
			SetNoteLength();
			break;
		case 8:
			SetStyle();
			break;
		case 9:
			SetOctave();
			break;
		case 10:
			SetAccent();
			break;
		case 11:
			SetVolumeVariation();
			break;
		case 12:
			m_score = Score();
			break;
		case 13:
			Randomize();
			break;
		default:
			break;
		}
	}
}

void Application::SetIntrument()
{
	m_gui.PrintInstruments();
	int choice = GetNumericInput("", 1, 4);

	if (choice == 4)
		m_score.SetRandomInstrument(true);
	else
		m_score.SetInstrument(choice);
}

void Application::SetKey()
{
	m_gui.PrintKeys();
	short keyIndex = GetValidKeyIndex();

	//Root note is offset from C, i.e. index of c# is 1 or midi note c#
	m_score.SetRoot(keyIndex);
}

void Application::SetScale()
{
	m_gui.PrintScale();
	int choice = GetNumericInput("", 1, 3);

	switch (choice)
	{
	case 1:
		m_score.SetScale(MAJOR_SCALE_MIDI);
		break;
	case 2:
		m_score.SetScale(MINOR_SCALE_MIDI);
		break;
	case 3:
		SetCustomScale();
		break;
	default:
		break;
	}
}

void Application::SetCustomScale()
{
	m_gui.PrintCustomScale();
	std::vector<unsigned short> customScale;
	int note;

	for (int i = 0; i < 8; ++i)
	{		
		note = GetNumericInput("", 0, 12);
		customScale.push_back(note);
	}

	m_score.SetScale(customScale);
}

void Application::SetLength()
{
	m_gui.PrintLength();
	int choice = GetNumericInput("", 1, 120);
	m_score.SetLength(choice);

}

void Application::SetTempo()
{
	m_gui.PrintTempo();
	int choice = GetNumericInput("", 20, 360);
	m_score.SetTempo(choice);
}

void Application::SetRate()
{
	m_gui.PrintRate();
	int choice = GetNumericInput("", 1, 8);
	m_score.SetRate(choice);
}

void Application::SetNoteLength()
{
	m_gui.PrintNoteLength();
	int choice = GetNumericInput("", 1, 100);
	m_score.SetNoteLength(choice / 100.f);
}

void Application::SetStyle()
{
	m_gui.PrintStyle();
	int choice = GetNumericInput("", 1, 5);
	m_score.SetStyle(static_cast<Score::Style>(choice-1));
}

void Application::SetOctave()
{
	m_gui.PrintOctaves();
	m_score.SetOct(GetNumericInput("", 1, 8) * 12);
}

void Application::SetAccent()
{
	m_gui.PrintAccent();
	m_score.SetAccent(GetNumericInput("", 0, 100));
}

void Application::SetVolumeVariation()
{
	m_gui.PrintVolumeVariation();
	int choice = GetNumericInput("", 0, 100);
	m_score.SetAmplitudeVariation(float(choice) / 100.f);
}

void Application::Randomize()
{
	m_gui.PrintRandomize();
	// Randomize all Parameters except for score length
	m_score.SetInstrument(rand() % 4 + 1);
	m_score.SetRoot(rand() % 12);

	if (rand() % 100 < 50)
		m_score.SetScale(MINOR_SCALE_MIDI);
	else
		m_score.SetScale(MAJOR_SCALE_MIDI);

	m_score.SetTempo(rand() % 360 + 21);
	m_score.SetRate(rand() % 8 + 1);

	m_score.SetNoteLength((rand() % 100 + 1) / 100.f);
	m_score.SetStyle(static_cast<Score::Style>(rand() % 4));
	m_score.SetOct((rand() % 8 + 1) * 12);
	m_score.SetAccent((rand() % 32));
	m_score.SetAmplitudeVariation(rand() % 50 / 100.f);
}

#pragma endregion
