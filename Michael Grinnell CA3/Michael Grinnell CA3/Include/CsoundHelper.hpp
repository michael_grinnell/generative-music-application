#pragma once

#include "Utility.hpp"
#include "csound.hpp"
#include <string>
#include <vector>

class CsoundHelper
{
public:
	CsoundHelper();
	~CsoundHelper();

	void Reset();

	void SetScoreString(std::string score);

	void SaveFile(std::string fileName);
	void PlayScore();

private:
	std::string m_orc;
	std::string m_sco;
	Csound* m_csound;

};

