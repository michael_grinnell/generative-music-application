#pragma once
#include "CsoundHelper.hpp"
#include "Score.hpp"
#include "GUI.hpp"

class Application
{
public:
	Application();
	void run();
	void Test();

private:
	void ProcessIput();
	void CreateScore();
	void SetIntrument();
	void SetKey();
	void SetScale();
	void SetCustomScale();
	void SetLength();
	void SetTempo();
	void SetRate();
	void SetNoteLength();
	void SetStyle();
	void SetOctave();
	void SetAccent();
	void SetVolumeVariation();
	void Randomize();
	void CompileScore();
	void SaveWav();

	int GetMidiNote(int i, bool* reverse);
	int GetScaleIndex(int i, bool reverse);
	float GetNoteAmplitude(int i);	
private:
	CsoundHelper m_csoundHelper;
	Score m_score;
	GUI m_gui;
	bool m_isRunning;
};