#pragma once
#include <string>

int GetNumericInput(const char* message, int min, int max);
bool is_number(const std::string& s);
short GetValidKeyIndex();