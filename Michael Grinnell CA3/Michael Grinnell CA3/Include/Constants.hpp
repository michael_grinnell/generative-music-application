#pragma once
#include <string>
#include <vector>

const std::vector<std::string> NOTES = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };

													 //R,W,W,H,W,W,W,H
const std::vector<unsigned short> MAJOR_SCALE_MIDI = { 0,2,4,5,7,9,11,12};
													 //R,W,H,W,W,H,W,W
const std::vector<unsigned short> MINOR_SCALE_MIDI = { 0,2,3,5,7,8,10,12 };


//Major Scale : R, W, W, H, W, W, W, H
//Natural Minor Scale : R, W, H, W, W, H, W, W
//Harmonic Minor Scale : R, W, H, W, W, H, 1 1 / 2, H(notice the stepand a half)
//Melodic Minor Scale : going up is : R, W, H, W, W, W, W, H
//going down is : R, W, W, H, W, W, H, W
//Dorian Mode is : R, W, H, W, W, W, H, W
//Mixolydian Mode is : R, W, W, H, W, W, H, W
//Ahava Raba Mode is : R, H, 1 1 / 2, H, W, H, W, W
//A minor pentatonic blues scale(no sharped 5) is : R, 1 1 / 2, W, W, 1 1 / 2, W


