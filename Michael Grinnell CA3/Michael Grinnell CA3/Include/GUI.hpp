#pragma once
#include "Score.hpp"

class GUI
{
public:
	GUI();
public:
	void PrintMain();
	void PrintCreate();
	void PrintSave();
	void PrintScore(Score score);
	void PrintInstruments();
	void PrintKeys();
	void PrintScale();
	void PrintCustomScale();
	void PrintLength();
	void PrintTempo();
	void PrintRate();
	void PrintNoteLength();
	void PrintStyle();
	void PrintOctaves();
	void PrintAccent();
	void PrintVolumeVariation();
	void PrintRandomize();
};