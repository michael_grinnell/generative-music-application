#pragma once
#include<vector>
#include<string>
class Score
{
public:
	Score();

	enum class Style
	{
		UP,
		DOWN,
		UPDOWN,
		DOWNUP,
		RANDOM
	};

	void SetRoot(unsigned short root);
	void SetScale(const std::vector<unsigned short> scale);
	void SetInstrument(unsigned short instrument);
	void SetLength(float length);
	void SetTempo(float tempo);
	void SetRate(float rate);
	void SetNoteLength(float length);
	void SetRandomInstrument(bool randomInstrument);
	void SetOctaves(std::vector<unsigned short> octaves);
	void SetStyle(Style style);
	void SetOct(int oct);
	void SetAccent(int accent);
	void SetAmplitudeVariation(float variation);

	unsigned short GetRoot();
	std::vector<unsigned short> GetScale();
	unsigned short GetInstrument();
	float GetLength();
	float GetTempo();
	float GetBPS();
	float GetNotes();
	float GetRate();
	float GetNoteLength();
	int GetOct();
	Score::Style GetStyle();
	float GetAmplitude();
	float GetAmplitudeVariation();
	unsigned short GetAccent();
	bool isRandomInstrument();
	std::string ToString();

private:
	std::vector<unsigned short> m_scale; 

	unsigned short m_root;
	unsigned short m_instrument;
	unsigned short m_accent;

	float m_length;
	float m_tempo;
	float m_rate;
	float m_noteLength;
	float m_ampVariation;
	float m_amplitude;
	
	bool m_randomInstrument;
	int m_octave;

	Style m_style;
};