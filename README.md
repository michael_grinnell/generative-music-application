# Introduction

For this project, I wanted to create a simple application that would allow the user to specify as many parameters as possible to create a unique score each time the application is run. I decided I would focus on timing, melody, and timbre, I would not cover harmony as this would take too much time to implement. To get started I took inspiration from Ableton Lives built-in Arpeggiator. This Midi effect takes midi notes as input and plays them in a rhythmic pattern. This pattern is defined by the parameters set by the user that control the speed length and pattern of the notes.

## Overview

Each time the application is run a score object is loaded with default parameters. This is the score that the user is working with for the duration of the application. When the application first loads the user is presented with the main menu. From here they can create/edit the current score, save the score as a Wav file and they can view the parameters that are set for the current score.
All the parameters of the score that can be modified are set from the create/edit score menu. These parameters should be easily understandable from their names. When you select a parameter to modify, a brief description of how it works is printed to screen. 